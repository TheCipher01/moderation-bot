const Discord = require("discord.js");
const fs = require("fs");
const ms = require("ms");
let warns = JSON.parse(fs.readFileSync("./warnings.json", "utf8"));

module.exports.run = async (bot, message, args) => {
    if(!message.member.hasPermission("MANAGE_MEMBERS")) return message.reply("You cannot do this!");
    let wUser = message.guild.member(message.mentions.users.first() || message.guild.members.get(args[0]));
    if(!wUser) return message.reply("Couldn't find that user");
    if(wUser.hasPermission("MANAGE_MESSAGES")) return message.reply("That user cannot be warned");
    let reason = args.join(" ").slice(22);

    if(!warns[wUser.id]) warns[wUser.id] = {
        warns: 0
    };

    warns[wUser.id].warns++;

    fs.writeFile("./warnigns.json", JSON.stringify(warns), (err) => {
        if(err) console.log(err)
    });

    let warnEmbed = new Discord.RichEmbed()
    .setDescription("Warns")
    .setAuthor(message.author.username)
    .setColor("#fc6400")
    .addField("Warned User:", `<@${wUser.id}>`)
    .addField("Warned In:", message.channel)
    .addField("Number of Warnings", warns[wUser.id].warns)
    .addField("Reason:", reason);

    let warnchannel = message.guild.channels.find(x => x.name === "incidents");
    if(!warnchannel) return message.reply("Couldn't find channel named `incidents`");

    message.delete().catch(O_o=>{});
    warnchannel.send(warnEmbed);

    if(warns[wUser.id].warns == 2){
        let muterole = message.guild.roles.find(x => x.name === "muted")
        if(!muterole) return message.reply("Role not found please create muted role")

        let muteTime = "120s";
        await(wUser.addRole(muterole.id));
        message.channel.send(`<@${wUser.id}> has been temporarily muted`);

        setTimeout(function(){
            wUser.removeRole(muterole.id)
            message.reply("They have been unmuted.")
        }, ms(muteTime))
    }
}
module.exports.help = {
    name: "warn"
}