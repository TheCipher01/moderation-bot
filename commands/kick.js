const Discord = require("discord.js");

module.exports.run = async(bot, message, args) => {
    let kUser = message.guild.member(message.mentions.users.first() || message.guild.members.get(args[0]));
    if(!kUser) return message.channel.send("Couldn't find user");
    let kReason = args.join(" ").slice(22);
    if(!message.member.hasPermission("MANAGE_MESSAGES")) return message.reply("You cannot do this")
    if(message.member.hasPermission("MANAGE_MESSAGES")) return message.reply("That person cannot be kicked!");

    let kickEmbed = new Discord.RichEmbed()
    .setDescription("~Kick~")
    .setColor("#e56b00")
    .addField("Kick User:", `${kUser} with ID ${kUser.id}`)
    .addField("Kicked By:", `<@${message.author.id}> with ID ${message.author.id}`)
    .addField("Time:", message.createdAt)
    .addField("Reason", kReason);

    let kickChannel = message.guild.channels.find(x => x.name === "incidents");
    if(!kickChannel) return message.reply("Can't find incidents channel")

    message.delete().catch(O_o=>{});
    message.guild.member(kUser).kick(kReason);
    kickChannel.send(kickEmbed);


}

module.exports.help = {
    name: "kick"
}