const Discord = require("discord.js");

module.exports.run = async (bot, message, args) => {
    let bUser = message.guild.member(message.mentions.users.first() || message.guild.members.get(args[0]));
    if(!bUser) return message.channel.send("Couldn't find user!");
    let bReason = args.join(" ").slice(22);
    if(!message.member.hasPermission("MANAGE_MESSAGES")) return message.channel.send("No can do");
    if (bUser.hasPermission("MANAGE_MESSAGES")) return message.channel.send("That person cnanot be banned!");

    let bannedEmbed = new Discord.RichEmbed()
    .setDescription("Ban")
    .setColor("#ff0000")
    .addField("Banned User:", `${bUser} with ID ${message.author.id}`)
    .addField("Banned By:", `<@${message.author.id}> with ID ${message.author.id}`)
    .addField("Banned In:", message.channel)
    .addField("Time:", message.createdAt)
    .addField("Reason:", bReason);

    let banChannel = message.guild.channels.find(x => x.name === "incidents");
    if(!banChannel) return message.channel.send("Can't find incidents channel");

    message.delete().catch(O_o=>{});
    message.guild.member(bUser).ban(bReason);
    banChannel.send(bannedEmbed);

    return;
}